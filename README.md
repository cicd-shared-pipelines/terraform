# Terraform Shared Pipelines

A collection of CICD pipelines specific to deploying Terraform. These pipelines will deploy your Terraform infrastructure as code into a designated cloud provider.

Currently this set up is tested against AWS Cloud Provider.

## Pipelines

The pipelines included in the project and what they can be used for is outlined in the table below.

| Name | Description | Dependencies |
| --- | --- | --- |
| build/.tfplan-gitlab-ci.yml | Performs the `terraform plan` command and outputs a file called `tfplan.binary`. | [`init`] |
| deploy/.tfdeploy-gitlab-ci.yml | Performs the `terraform apply` command based on the artifcate `tfplan.binary`. | [`tfplan`, `checkov`, `opa`, `tfvalidate`] |
| destroy/.tfdestroy-gitlab-ci.yml | Performs the `terraform destroy` command. | [`tfdeploy`] |
| prebuild/.tfinit-gitlab-ci.yml | Performs the `terraform init` command. | `None` |
| validation/.checkov-gitlab-ci.yml | Performs the static analysis of the Terraform code. | `None` |
| validation/.opa-gitlab-ci.yml | Performs policy based enforcement. | `None` |
| validation/.tfvalidate-gitlab-ci.yml | Performs the `terraform validate` command. | [`init`]
| validation/.tffmt-gitlab-ci.yml | Performs the `terraform fmt` command. | [`init`]

## Usage

```yml

include:
  # Prebuild Stages
  - https://gitlab.com/cicd-shared-pipelines/terraform/-/raw/master/prebuild/.tfinit-gitlab-ci.yml
  # Validation Stages
  - https://gitlab.com/cicd-shared-pipelines/terraform/-/raw/master/validation/.checkov-gitlab-ci.yml
  - https://gitlab.com/cicd-shared-pipelines/terraform/-/raw/master/validation/.tffmt-gitlab-ci.yml
  - https://gitlab.com/cicd-shared-pipelines/terraform/-/raw/master/validation/.tfvalidate-gitlab-ci.yml
  # Build Stage
  - https://gitlab.com/cicd-shared-pipelines/terraform/-/raw/master/build/.tfplan-gitlab-ci.yml
  # Deploy Stage
  - https://gitlab.com/cicd-shared-pipelines/terraform/-/raw/master/deploy/.tfdeploy-gitlab-ci.yml
  - https://gitlab.com/cicd-shared-pipelines/terraform/-/raw/master/deploy/.tfdestroy-gitlab-ci.yml

stages:
  - prebuild
  - validation
  - build
  - deploy

# Images and Variables
image: frankmartinez1/tf-aws:0.0.3
variables:
  AWS_DEFAULT_REGION: us-east-1

# DAG Configurations
checkov:
  needs: []

tfvalidate:
  needs: ["tfinit"]

tffmt:
  needs: []

tfplan:
  needs: ["tfinit"]

tfdeploy:
  needs: ["tfplan", "checkov", "tfvalidate"]

```
